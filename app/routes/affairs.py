from dataclasses import asdict
from datetime import datetime, timedelta
from http import client

from dacite import from_dict
from flask import Blueprint, abort, redirect, render_template, request, url_for

from app.crud.affair import (
    add_person,
    add_resource,
    del_person_from_affair,
    del_resource_from_affair,
    delete_affair,
    get_affair,
    get_affairs,
    get_people_links_for_affair,
    get_resource_links_for_affair,
    insert_affair,
    mark_affair_done,
    update_affair,
)
from app.crud.category import get_categories
from app.crud.exceptions import AffairNotFound, PersonNotFound, ResourceNotFound
from app.crud.person import get_person
from app.crud.resource import get_resource
from app.forms.add_person_to_affair import PersonAffairForm
from app.forms.add_resource_to_affair import ResourceAffairForm
from app.forms.affair import AffairForm
from app.models.affair import AffairInput, AffairPeriod, AffairPriority

affairs_bp = Blueprint("affairs", __name__)


@affairs_bp.route("/")
def affairs():
    args = request.args
    from_date = args.get("from_date_time", "")
    to_date = args.get("to_date_time", "")
    is_open = args.get("is_open")
    from_date = (
        datetime.strptime(from_date, "%Y-%m-%dT%H:%M")
        if from_date != ""
        else datetime(1900, 1, 1)
    )
    to_date = (
        datetime.strptime(to_date, "%Y-%m-%dT%H:%M")
        if to_date != ""
        else datetime(2100, 1, 1)
    )
    return render_template(
        "affairs.html.j2",
        affairs=get_affairs(from_date, to_date, is_open),
        from_date_time=args.get("from_date_time", ""),
        to_date_time=args.get("to_date_time", ""),
        is_open=args.get("is_open"),
    )


@affairs_bp.route("/<int:affair_id>")
def affair(affair_id):
    try:
        affair = get_affair(affair_id)
    except AffairNotFound:
        abort(client.NOT_FOUND)
    return render_template(
        "affair.html.j2",
        affair=affair,
        id=affair_id,
        affair_period_enum=AffairPeriod,
        affair_priority_enum=AffairPriority,
    )


@affairs_bp.route("/<int:affair_id>/mark_done")
def affair_done(affair_id):
    try:
        affair = get_affair(affair_id)
    except AffairNotFound:
        abort(client.NOT_FOUND)
    if affair.done_percent != 100:
        if affair.period and affair.linked_time:
            affair.linked_time += timedelta(seconds=affair.period)
            affair.done_percent = 0
            while affair.linked_time < datetime.now():
                affair.linked_time += timedelta(seconds=affair.period)
            new_affair_id = insert_affair(
                from_dict(data_class=AffairInput, data=asdict(affair))
            )
            for resource in affair.resources:
                add_resource(new_affair_id, resource.id, resource.quantity)
            for person in affair.people:
                add_person(new_affair_id, person.id)
        mark_affair_done(affair_id)
    return redirect(url_for(".affairs"))


@affairs_bp.route("/<int:affair_id>/add_person", methods=("GET", "POST"))
def add_person_to_affair(affair_id):
    try:
        get_affair(affair_id)
    except AffairNotFound:
        abort(client.NOT_FOUND)
    form = PersonAffairForm()
    form.person_id.choices = [
        (person.id, " ".join(filter(None, (person.first_name, person.last_name))))
        for person in get_people_links_for_affair(affair_id)
    ]
    if form.validate_on_submit():
        person_id = form.person_id.data
        add_person(affair_id, person_id)
        return redirect(url_for(".affair", affair_id=affair_id))
    return render_template("add_person_to_affair.html.j2", form=form)


@affairs_bp.route(
    "/<int:affair_id>/delete_person/<int:person_id>", methods=("GET", "POST")
)
def delete_person_from_affair(affair_id, person_id):
    if request.method == "GET":
        try:
            affair = get_affair(affair_id)
            person = get_person(person_id)
        except (AffairNotFound, PersonNotFound):
            abort(client.NOT_FOUND)
        return render_template(
            "delete_person_from_affair.html.j2",
            affair=affair,
            person=person,
            affair_id=affair_id,
        )
    else:
        del_person_from_affair(affair_id, person_id)
    return redirect(url_for(".affair", affair_id=affair_id))


@affairs_bp.route("/<int:affair_id>/delete", methods=("GET", "POST"))
def del_affair(affair_id):
    if request.method == "GET":
        try:
            affair = get_affair(affair_id)
        except AffairNotFound:
            abort(client.NOT_FOUND)
        return render_template(
            "delete_affair.html.j2", affair=affair, affair_id=affair_id
        )
    else:
        delete_affair(affair_id)
        return redirect(url_for(".affairs"))


@affairs_bp.route("/<int:affair_id>/add_resource", methods=("GET", "POST"))
def add_resource_to_affair(affair_id):
    try:
        get_affair(affair_id)
    except AffairNotFound:
        abort(client.NOT_FOUND)
    form = ResourceAffairForm()
    form.resource_id.choices = [
        (resource.id, resource.name)
        for resource in get_resource_links_for_affair(affair_id)
    ]
    if form.validate_on_submit():
        resource_id = form.resource_id.data
        quantity = form.quantity.data
        add_resource(affair_id, resource_id, quantity)
        return redirect(url_for(".affair", affair_id=affair_id))
    return render_template("add_resource_to_affair.html.j2", form=form)


@affairs_bp.route(
    "/<int:affair_id>/delete_resource/<int:resource_id>", methods=("GET", "POST")
)
def delete_resource_from_affair(affair_id, resource_id):
    if request.method == "GET":
        try:
            affair = get_affair(affair_id)
            resource = get_resource(resource_id)
        except (AffairNotFound, ResourceNotFound):
            abort(client.NOT_FOUND)
        return render_template(
            "delete_resource_from_affair.html.j2",
            affair=affair,
            resource=resource,
            affair_id=affair_id,
        )
    else:
        del_resource_from_affair(affair_id, resource_id)
    return redirect(url_for(".affair", affair_id=affair_id))


@affairs_bp.route("/new", methods=("GET", "POST"))
def new_affair():
    form = AffairForm()
    form.category_id.choices = [
        (category.id, category.name) for category in get_categories()
    ]
    form.category_id.choices.append((-1, "Без Категории"))
    if form.validate_on_submit():
        f = dict(**form.data)
        f["period"] = None if f["period"] == 0 else f["period"]
        f["category_id"] = None if f["category_id"] == -1 else f["category_id"]
        affair_id = insert_affair(from_dict(data_class=AffairInput, data=f))
        return redirect(url_for(".affair", affair_id=affair_id))
    return render_template("new_affair.html.j2", form=form)


@affairs_bp.route("/<int:affair_id>/edit", methods=("GET", "POST"))
def edit_affair(affair_id):
    try:
        affair = get_affair(affair_id)
    except AffairNotFound:
        abort(client.NOT_FOUND)
    form = AffairForm(obj=affair)
    form.category_id.choices = [
        (category.id, category.name) for category in get_categories()
    ]
    form.category_id.choices.append((-1, "Без Категории"))
    if form.validate_on_submit():
        f = dict(**form.data)
        f["period"] = None if f["period"] == 0 else f["period"]
        f["category_id"] = None if f["category_id"] == -1 else f["category_id"]
        update_affair(from_dict(data_class=AffairInput, data=f), affair_id)
        return redirect(url_for(".affair", affair_id=affair_id))
    return render_template("edit_affair.html.j2", form=form)


@affairs_bp.route("/<int:affair_id>/new_sub_affair", methods=("GET", "POST"))
def new_sub_affair(affair_id):
    try:
        get_affair(affair_id)
    except AffairNotFound:
        abort(client.NOT_FOUND)
    form = AffairForm()
    form.category_id.choices = [
        (category.id, category.name) for category in get_categories()
    ]
    form.category_id.choices.append((-1, "Без Категории"))
    if form.validate_on_submit():
        f = dict(**form.data)
        f["period"] = None if f["period"] == 0 else f["period"]
        f["category_id"] = None if f["category_id"] == -1 else f["category_id"]
        affair_id = insert_affair(
            from_dict(data_class=AffairInput, data=f), parent_affair_id=affair_id
        )
        return redirect(url_for(".affair", affair_id=affair_id))
    return render_template("new_affair.html.j2", form=form)
