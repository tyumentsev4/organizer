from http import client
from dacite import from_dict
from flask import Blueprint, abort, redirect, render_template, request, url_for
from app.crud.exceptions import PersonNotFound

from app.crud.person import (
    delete_person,
    get_people_links,
    get_person,
    insert_person,
    update_person,
)
from app.forms.person import PersonForm
from app.models.person import Person


people_bp = Blueprint("people", __name__)


@people_bp.route("/")
def people():
    return render_template("people.html.j2", people=get_people_links())


@people_bp.route("/new", methods=("GET", "POST"))
def new_person():
    form = PersonForm()
    if form.validate_on_submit():
        person_id = insert_person(from_dict(data_class=Person, data=form.data))
        return redirect(url_for(".person", person_id=person_id))
    return render_template("new_person.html.j2", form=form)


@people_bp.route("/<int:person_id>")
def person(person_id):
    try:
        person = get_person(person_id)
    except PersonNotFound:
        abort(client.NOT_FOUND)
    return render_template("person.html.j2", person=person, person_id=person_id)


@people_bp.route("/<int:person_id>/delete", methods=("GET", "POST"))
def del_person(person_id):
    if request.method == "GET":
        try:
            person = get_person(person_id)
        except PersonNotFound:
            abort(client.NOT_FOUND)
        return render_template(
            "delete_person.html.j2", person=person, person_id=person_id
        )
    else:
        delete_person(person_id)
        return redirect(url_for(".people"))


@people_bp.route("/<int:person_id>/edit", methods=("GET", "POST"))
def edit_person(person_id):
    try:
        person = get_person(person_id)
    except PersonNotFound:
        abort(client.NOT_FOUND)
    form = PersonForm(obj=person)
    if form.validate_on_submit():
        update_person(from_dict(data_class=Person, data=form.data), person_id)
        return redirect(url_for(".person", person_id=person_id))
    return render_template("edit_person.html.j2", form=form)
