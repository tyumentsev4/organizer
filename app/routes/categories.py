from http import client

from dacite import from_dict
from flask import Blueprint, abort, redirect, render_template, request, url_for

from app.crud.category import (
    delete_category,
    get_categories,
    get_category,
    insert_category,
    update_category,
)
from app.crud.exceptions import CategoryNotFound
from app.forms.category import CategoryForm
from app.models.category import Category

categories_bp = Blueprint("categories", __name__)


@categories_bp.route("/new", methods=("GET", "POST"))
def new_category():
    form = CategoryForm()
    if form.validate_on_submit():
        insert_category(form.name.data)
        return redirect(url_for("categories.categories"))
    return render_template("new_category.html.j2", form=form)


@categories_bp.route("/")
def categories():
    return render_template("categories.html.j2", categories=get_categories())


@categories_bp.route("/<int:category_id>/delete", methods=("GET", "POST"))
def del_category(category_id):
    if request.method == "GET":
        try:
            category = get_category(category_id)
        except CategoryNotFound:
            abort(client.NOT_FOUND)
        return render_template("delete_category.html.j2", category=category)
    else:
        delete_category(category_id)
        return redirect(url_for(".categories"))


@categories_bp.route("/<int:category_id>/edit", methods=("GET", "POST"))
def edit_category(category_id):
    try:
        category = get_category(category_id)
    except CategoryNotFound:
        abort(client.NOT_FOUND)
    form = CategoryForm(obj=category)
    if form.validate_on_submit():
        update_category(from_dict(data_class=Category, data=form.data), category_id)
        return redirect(url_for(".categories"))
    return render_template("edit_category.html.j2", form=form)
