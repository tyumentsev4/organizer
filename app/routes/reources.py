from http import client
from dacite import from_dict
from flask import Blueprint, abort, redirect, render_template, request, url_for
from app.crud.exceptions import ResourceNotFound

from app.crud.resource import (
    delete_resource,
    get_resource,
    get_resources_links,
    insert_resource,
    update_resource,
)
from app.forms.resource import ResourceForm
from app.models.resource import Resource


resources_bp = Blueprint("resources", __name__)


@resources_bp.route("/")
def resources():
    return render_template("resources.html.j2", resources=get_resources_links())


@resources_bp.route("/<int:resource_id>")
def resource(resource_id):
    try:
        resource = get_resource(resource_id)
    except ResourceNotFound:
        abort(client.NOT_FOUND)
    return render_template(
        "resource.html.j2", resource=resource, resource_id=resource_id
    )


@resources_bp.route("/new", methods=("GET", "POST"))
def new_resource():
    form = ResourceForm()
    if form.validate_on_submit():
        resource_id = insert_resource(from_dict(data_class=Resource, data=form.data))
        return redirect(url_for(".resource", resource_id=resource_id))
    return render_template("new_resource.html.j2", form=form)


@resources_bp.route("/<int:resource_id>/delete", methods=("GET", "POST"))
def del_resource(resource_id):
    if request.method == "GET":
        try:
            resource = get_resource(resource_id)
        except ResourceNotFound:
            abort(client.NOT_FOUND)
        return render_template(
            "delete_resource.html.j2", resource=resource, resource_id=resource_id
        )
    else:
        delete_resource(resource_id)
        return redirect(url_for(".resources"))


@resources_bp.route("/<int:resource_id>/edit", methods=("GET", "POST"))
def edit_resource(resource_id):
    try:
        resource = get_resource(resource_id)
    except ResourceNotFound:
        abort(client.NOT_FOUND)
    form = ResourceForm(obj=resource)
    if form.validate_on_submit():
        update_resource(from_dict(data_class=Resource, data=form.data), resource_id)
        return redirect(url_for(".resource", resource_id=resource_id))
    return render_template("edit_resource.html.j2", form=form)
