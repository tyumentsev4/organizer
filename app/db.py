import os

import oracledb
from flask import g


def get_db():
    if "db" not in g:
        g.db = oracledb.connect(
            user="organizer",
            password="password",
            host=os.getenv("DB_HOST", "localhost"),
            disable_oob=True,
            service_name="xepdb1",
        )
    return g.db


def close_db(e=None):
    db = g.pop("db", None)

    if db is not None:
        db.rollback()
        db.close()
