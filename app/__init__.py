from flask import Flask, redirect, render_template, request

from app.config import Config
from app.db import close_db
from app.routes.affairs import affairs_bp
from app.routes.categories import categories_bp
from app.routes.people import people_bp
from app.routes.reources import resources_bp

app = Flask(__name__)
app.config.from_object(Config)

app.register_blueprint(affairs_bp, url_prefix="/affairs")
app.register_blueprint(people_bp, url_prefix="/people")
app.register_blueprint(resources_bp, url_prefix="/resources")
app.register_blueprint(categories_bp, url_prefix="/categories")


@app.route("/")
def root():
    return redirect("/affairs")


@app.teardown_request
def teardown_request(exception):
    if exception:
        close_db(exception)


@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html.j2", url=request.url), 404
