from dataclasses import asdict
from typing import Generator

from app.crud.crud_utils import read_sql_query
from app.crud.exceptions import ResourceNotFound
from app.db import get_db
from app.models.resource import Resource, ResourceLink


def insert_resource(resource: Resource) -> int:
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/insert_resource.sql")
        resource_id_var = curs.var(int)
        curs.execute(raw_sql, resource_id=resource_id_var, **asdict(resource))
        resource_id = resource_id_var.getvalue()[0]
    conn.commit()
    return resource_id


def get_resources_links() -> Generator[ResourceLink, None, None]:
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/get_resources.sql")
        for row in curs.execute(raw_sql):
            yield ResourceLink(*row)
        return


def get_resource(resource_id: int) -> Resource:
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/get_resource.sql")
        curs.execute(raw_sql, resource_id=resource_id)
        resource = curs.fetchone()
        if resource is None:
            raise ResourceNotFound()
        return Resource(*resource)


def delete_resource(resource_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/delete_resource.sql")
        curs.execute(raw_sql, resource_id=resource_id)
    conn.commit()


def update_resource(resource: Resource, resource_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/update_resource.sql")
        curs.execute(raw_sql, **asdict(resource), resource_id=resource_id)
    conn.commit()
