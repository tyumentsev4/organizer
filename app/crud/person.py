from dataclasses import asdict
from typing import Generator

from app.crud.crud_utils import read_sql_query
from app.crud.exceptions import PersonNotFound
from app.db import get_db
from app.models.person import Person, PersonLink


def get_people_links() -> Generator[PersonLink, None, None]:
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/get_people.sql")
        for row in curs.execute(raw_sql):
            yield PersonLink(*row)
        return


def get_person(person_id: int) -> Person:
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/get_person.sql")
        curs.execute(raw_sql, person_id=person_id)
        person = curs.fetchone()
        if person is None:
            raise PersonNotFound()
        return Person(*person)


def insert_person(person: Person) -> int:
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/insert_person.sql")
        person_id_var = curs.var(int)
        curs.execute(raw_sql, person_id=person_id_var, **asdict(person))
        person_id = person_id_var.getvalue()[0]
    conn.commit()
    return person_id


def delete_person(person_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/delete_person.sql")
        curs.execute(raw_sql, person_id=person_id)
    conn.commit()


def update_person(person: Person, person_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/update_person.sql")
        curs.execute(raw_sql, **asdict(person), person_id=person_id)
    conn.commit()
