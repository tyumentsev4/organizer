from dataclasses import asdict
from typing import Generator

from app.crud.crud_utils import read_sql_query
from app.crud.exceptions import AffairNotFound
from app.db import get_db
from app.models.affair import AffairFull, AffairInput, AffairLink
from app.models.person import PersonLink
from app.models.resource import ResourceByAffair, ResourceLink


def get_affairs(from_date, to_date, is_open) -> Generator[AffairLink, None, None]:
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/get_affairs.sql")
        if is_open == "true":
            raw_sql += " AND done_percent <> 100"
        elif is_open == "false":
            raw_sql += " AND done_percent = 100"
        raw_sql += " ORDER BY LINKED_TIME NULLS FIRST"
        for row in curs.execute(raw_sql, from_date=from_date, to_date=to_date):
            yield AffairLink(*row)
        return


def get_affair(affair_id: int) -> AffairFull:
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/get_affair.sql")
        curs.execute(raw_sql, affair_id=affair_id)
        affair = curs.fetchone()
        if affair is None:
            raise AffairNotFound()
        raw_sql = read_sql_query("app/sql/get_affair_people.sql")
        curs.execute(raw_sql, affair_id=affair_id)
        people = [PersonLink(*row) for row in curs.fetchmany()]
        raw_sql = read_sql_query("app/sql/get_affair_resources.sql")
        curs.execute(raw_sql, affair_id=affair_id)
        resources = [ResourceByAffair(*row) for row in curs.fetchmany()]
        affair = AffairFull(*affair, people=people, resources=resources)
        return affair


def insert_affair(affair: AffairInput, parent_affair_id=None) -> int:
    conn = get_db()
    if parent_affair_id is not None:
        affair.parent_affair_id = parent_affair_id
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/insert_affair.sql")
        affair_id_var = curs.var(int)
        curs.execute(raw_sql, affair_id=affair_id_var, **asdict(affair))
        affair_id = affair_id_var.getvalue()[0]
    conn.commit()
    return affair_id


def get_people_links_for_affair(affair_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/get_people_to_affair.sql")
        for row in curs.execute(raw_sql, affair_id=affair_id):
            yield PersonLink(*row)
        return


def get_resource_links_for_affair(affair_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/get_resource_to_affair.sql")
        for row in curs.execute(raw_sql, affair_id=affair_id):
            yield ResourceLink(*row)
        return


def add_resource(affair_id: int, resource_id: int, quantity: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/add_resource_to_affair.sql")
        curs.execute(
            raw_sql, affair_id=affair_id, resource_id=resource_id, quantity=quantity
        )
    conn.commit()


def del_resource_from_affair(affair_id: int, resource_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/delete_resource_to_affair.sql")
        curs.execute(raw_sql, affair_id=affair_id, resource_id=resource_id)
    conn.commit()


def add_person(affair_id: int, person_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/add_person_to_affair.sql")
        curs.execute(raw_sql, affair_id=affair_id, person_id=person_id)
    conn.commit()


def del_person_from_affair(affair_id: int, person_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/delete_person_to_affair.sql")
        curs.execute(raw_sql, affair_id=affair_id, person_id=person_id)
    conn.commit()


def delete_affair(affair_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/delete_affair.sql")
        curs.execute(raw_sql, affair_id=affair_id)
    conn.commit()


def update_affair(affair: AffairInput, affair_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/update_affair.sql")
        curs.execute(raw_sql, **asdict(affair), affair_id=affair_id)
    conn.commit()


def mark_affair_done(affair_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/mark_affair_done.sql")
        curs.execute(raw_sql, affair_id=affair_id)
    conn.commit()
