from pathlib import Path


def read_sql_query(sql_path: str) -> str:
    return Path(sql_path).read_text()
