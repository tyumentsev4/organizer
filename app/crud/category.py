from dataclasses import asdict
from typing import Generator

from app.crud.crud_utils import read_sql_query
from app.crud.exceptions import CategoryNotFound
from app.db import get_db
from app.models.category import Category


def insert_category(name: str):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/insert_category.sql")
        category_id_var = curs.var(int)
        curs.execute(raw_sql, name=name, category_id=category_id_var)
    conn.commit()


def get_categories() -> Generator[Category, None, None]:
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/get_categories.sql")
        for row in curs.execute(raw_sql):
            yield Category(*row)
        return


def get_category(category_id: int) -> Category:
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/get_category.sql")
        curs.execute(raw_sql, category_id=category_id)
        category = curs.fetchone()
        if category is None:
            raise CategoryNotFound()
        return Category(*category)


def delete_category(category_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/delete_category.sql")
        curs.execute(raw_sql, category_id=category_id)
    conn.commit()


def update_category(category: Category, category_id: int):
    conn = get_db()
    with conn.cursor() as curs:
        raw_sql = read_sql_query("app/sql/update_category.sql")
        curs.execute(raw_sql, **asdict(category), category_id=category_id)
    conn.commit()
