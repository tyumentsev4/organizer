class ResourceNotFound(Exception):
    pass


class PersonNotFound(Exception):
    pass


class AffairNotFound(Exception):
    pass


class CategoryNotFound(Exception):
    pass
