UPDATE AFFAIR
SET TITLE        = :title,
    DESCRIPTION  = :description,
    LINKED_TIME  = :linked_time,
    PERIOD       = :period,
    DONE_PERCENT = :done_percent,
    CATEGORY_ID  = :category_id,
    PRIORITY     = :priority,
    PARENT_AFFAIR_ID = :parent_affair_id
WHERE ID = :affair_id
