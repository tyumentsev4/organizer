SELECT ID, name
FROM "RESOURCE"
MINUS
SELECT ID, name
FROM "RESOURCE"
         JOIN affair_to_resource atr ON "RESOURCE".ID = atr.RESOURCE_ID
WHERE atr.affair_id = :affair_id
