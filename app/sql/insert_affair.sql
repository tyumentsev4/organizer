INSERT INTO AFFAIR (title, description, linked_time, period, done_percent, category_id, priority, parent_affair_id)
VALUES (:title, :description, :linked_time, :period, :done_percent, :category_id, :priority, :parent_affair_id)
RETURNING ID INTO :affair_id
