UPDATE PERSON
SET first_name  = :first_name,
    last_name   = :last_name,
    description = :description,
    contacts    = :contacts
WHERE ID = :person_id
