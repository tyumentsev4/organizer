SELECT ID, first_name, last_name
FROM PERSON
MINUS
SELECT ID, first_name, last_name
FROM PERSON
         JOIN affair_to_people atp ON person.id = atp.person_id
WHERE atp.affair_id = :affair_id
