from dataclasses import dataclass
from datetime import datetime

from aenum import Enum, MultiValueEnum

from app.models.person import PersonLink
from app.models.resource import ResourceByAffair


class AffairPriority(Enum):
    IMPORTANT = "Срочно"
    ORDINARY = "Обычный"
    UNIMPORTANT = "Несрочно"


class AffairPeriod(MultiValueEnum, init="sec label"):
    NEVER = 0, "Не повторять"
    DAY = 86400, "Каждый день"
    WEEK = 604800, "Каждую неделю"
    MONTH = 2678400, "Каждый месяц"
    YEAR = 31536000, "Каждый год"


@dataclass
class AffairLink:
    id: int  # noqa
    title: str
    priority: str
    linked_time: datetime | None
    done_percent: int
    category: str | None


@dataclass
class AffairInput:
    title: str
    description: str | None
    linked_time: datetime | None
    period: int | None
    category_id: int | None
    priority: str
    done_percent: int
    parent_affair_id: int | None


@dataclass
class AffairBreif(AffairInput):
    category: str | None


@dataclass
class AffairFull(AffairBreif):
    created_at: datetime
    parent_affair_title: str | None
    people: list[PersonLink]
    resources: list[ResourceByAffair]
