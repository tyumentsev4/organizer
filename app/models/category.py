from dataclasses import dataclass


@dataclass
class Category:
    id: int  # noqa
    name: str
