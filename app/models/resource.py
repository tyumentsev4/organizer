from dataclasses import dataclass


@dataclass
class Resource:
    name: str


@dataclass
class ResourceLink:
    id: int  # noqa
    name: str


@dataclass
class ResourceByAffair(ResourceLink):
    quantity: int
