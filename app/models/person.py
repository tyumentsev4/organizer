from dataclasses import dataclass


@dataclass
class Person:
    first_name: str
    last_name: str | None
    description: str
    contacts: str


@dataclass
class PersonLink:
    id: int  # noqa
    first_name: str
    last_name: str | None
