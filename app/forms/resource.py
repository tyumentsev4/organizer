from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length


class ResourceForm(FlaskForm):
    name = StringField(
        "Название",
        validators=(
            DataRequired("У ресурса должно быть название"),
            Length(max=100, message="Название не может быть длиннее 100 символов"),
        ),
    )
    submit = SubmitField("Создать", render_kw={"class": "button success"})
