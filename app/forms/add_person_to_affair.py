from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField
from wtforms.validators import DataRequired


class PersonAffairForm(FlaskForm):
    person_id = SelectField(
        "Человек",
        validators=(DataRequired("Выберите человека"),),
        coerce=int,  # type: ignore
    )
    submit = SubmitField("Создать", render_kw={"class": "button success"})
