from flask_wtf import FlaskForm
from wtforms import (
    DateTimeLocalField,
    IntegerField,
    SelectField,
    StringField,
    SubmitField,
    TextAreaField,
)
from wtforms.validators import DataRequired, NumberRange, Optional, Length

from app.models.affair import AffairPeriod, AffairPriority


class AffairForm(FlaskForm):
    title = StringField(
        "Название",
        validators=(
            DataRequired("Дело должно иметь название"),
            Length(max=50, message="Название не может быть длиннее 50 символов"),
        ),
    )
    description = TextAreaField(
        "Описание",
        validators=(
            Length(max=500, message="Описание не может быть длиннее 500 символов"),
        ),
    )
    linked_time = DateTimeLocalField(
        "Время", format="%Y-%m-%dT%H:%M", validators=(Optional(),)
    )
    period = SelectField(
        "Периодичность",
        validators=(Optional(),),
        validate_choice=False,
        choices=[(per.sec, per.label) for per in AffairPeriod],
        coerce=int,  # type: ignore
        default=AffairPeriod.NEVER.sec,  # type: ignore
    )
    done_percent = IntegerField(
        "Процент выполнения",
        default=0,
        validators=(NumberRange(0, 100, "Укажите число от 0 до 100"),),
    )
    priority = SelectField(
        "Приоритет",
        choices=[(pr.name, pr.value) for pr in AffairPriority],
        default=AffairPriority.ORDINARY.name,  # type: ignore
    )
    category_id = SelectField(
        "Категория",
        default=-1,
        validate_choice=False,
        coerce=int,  # type: ignore
    )
    submit = SubmitField("Создать", render_kw={"class": "button success"})
