from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Length


class PersonForm(FlaskForm):
    first_name = StringField(
        "Имя",
        validators=(
            DataRequired("Человек должен иметь имя"),
            Length(max=50, message="Имя не может быть длиннее 50 символов"),
        ),
    )
    last_name = StringField(
        "Фамилия",
        validators=(
            Length(max=50, message="Фамилия не может быть длиннее 50 символов"),
        ),
    )
    description = TextAreaField(
        "Описание",
        validators=(
            Length(max=500, message="Описание не может быть длиннее 500 символов"),
        ),
    )
    contacts = StringField(
        "Контактные данные",
        validators=(
            Length(
                max=100, message="Контактные данные не могут быть длиннее 100 символов"
            ),
        ),
    )
    submit = SubmitField("Создать", render_kw={"class": "button success"})
