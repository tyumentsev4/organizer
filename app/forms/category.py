from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length


class CategoryForm(FlaskForm):
    name = StringField(
        "Название",
        validators=(
            DataRequired(),
            Length(
                max=50, message="Название категории не может быть длиннее 50 символов"
            ),
        ),
    )
    submit = SubmitField("Создать", render_kw={"class": "button success"})
