from flask_wtf import FlaskForm
from wtforms import SelectField, SubmitField, IntegerField
from wtforms.validators import DataRequired, NumberRange


class ResourceAffairForm(FlaskForm):
    resource_id = SelectField(
        "Ресурс",
        validators=(DataRequired("Выберите человека"),),
        coerce=int,  # type: ignore
    )
    quantity = IntegerField(
        "Количество",
        validators=(NumberRange(1, 2000000000, "Введите число от 1 до 2000000000"),),
        default=1,
    )
    submit = SubmitField("Создать", render_kw={"class": "button success"})
