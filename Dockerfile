FROM python:3.11-slim 
COPY app /organizer/app/
COPY pyproject.toml /organizer/
WORKDIR /organizer
ENV PYTHONPATH=${PYTHONPATH}:${PWD} 
RUN pip3 install poetry
RUN poetry config virtualenvs.create false
RUN poetry install --only main
EXPOSE 8000

CMD ["gunicorn", "-b", "0.0.0.0", "app:app"]
